import pandas as pd


def filter_df(df, column, criteria):
    """
    Filter the input df by values of specific column
    :param df: input dataframe
    :param column: the specific column that want to use as the filer criteria
    :param criteria: the array with values in columns that want to be selected
    :return: new_df: dataframe
    """
    new_df = df[df[column].isin(criteria)]
    return new_df

def select_column(df, string):
    """
    select specific columns of a dataframe based on the starting string of column name
    :param df: dataframe
    :param string: the string that want to be selected as the begining of columns
    :return: df_select
    """
    df_new = df.loc[:, df.columns.str.startswith(string)]
    df_stimuli = df[['stimulus', 'choice.rating', 'HI_LO_fat', ]]
    df_select = pd.concat([df_stimuli, df_new], axis=1)
    return df_select

def drop_col(df, col):
    """
    Drop specified columns
    :param df: dataframe
    :param col: columns want to be dropped
    :return: dataframe with rest columns
    """
    df_new = df.drop(columns=col)
    return df_new


def merge_mean(df1, df2, group_by):
    """
    to calculate mean values of each rating, and combine the values
    with memorability data
    :param df1: df to calculate mean
    :param df2: df to be combined with later
    :param group_by: the column want to be goruped by in df1
    :return: calculated and combined df
    """
    df_mean = df1.groupby(group_by).mean()
    df_merged = pd.merge(df2, df_mean, how='left', on='stimulus')
    return df_merged


def name_replace_col(df, replace_from, replace_to):
    """
    function to change column names
    :param df: dataframe
    :param replace_from: string parameter of column names
    :param replace_to: string parameter want to be changed to
    :return:
    """
    df_new = df.columns.astype(str).str.replace(replace_from, replace_to)
    return  df_new

def reorder(df):
    """
    reorder the columns for better plot
    :param df:
    :return:
    """
    col = ["Memorability", "tasty", "othertasty", "feel", "texture", "disgusting", "familiar", "filling", "healthy",
           "sweetsaltysavory", "calories", "carbohydrates", "fat", "vitamins", "gluten", "sodium", "sugar", "protein"]
    df_new = df[col]
    return df_new