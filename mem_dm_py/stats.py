from scipy.stats import pearsonr

def cor_test(variable1, variable2):
    """
    Calculate correlational coefficients
    :param variable1: first variable
    :param variable2: second variable
    :return:
    """
    corr, p = pearsonr(variable1, variable2)
    print('Pearsons correlation: %.3f' % corr + ', p-value: %.3f' % p)
