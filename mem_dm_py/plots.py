import numpy as np
import seaborn as sns
from bokeh.io import show
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from matplotlib import pyplot as plt


def histogram(df, x):
    """
    Plot a histogram with given dataframe and x
    :param df: dataframe
    :param x: x that want to plot
    :return:
    """
    sns.set_theme()
    sns.histplot(data=df, x=x, binwidth=0.05)


def corr_matrix(df):
    """
    Return a correlational matrix with all of the columns in df
    :param df: dataframe
    :return: plot of correlational matrix
    """
    corr = df.corr()

    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(corr, dtype=bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(14, 14))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.set(font_scale=1.0)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1, center=0,
                square=True, linewidths=.5, annot=True)


def bokeh_scatter(df, plot_x, plot_y, plot_title, plot_category, plot_tooltips,
                  cmap, legend_location,
                  plot_width_pix=600,
                  plot_height_pix=450,
                  plot_tools = 'box_zoom,pan,save,hover,reset,zoom_in, zoom_out' ):

    """
    create bokeh scatter plot with parameters
    :param df: dataframe
    :param plot_x: x label
    :param plot_y: y label
    :param plot_title: plot title
    :param plot_category: plot category
    :param plot_tooltips: hover tips
    :param cmap: color map
    :param legend_location: location of legend
    :return: a Bokeh scatter plot
    """

    # plot
    p = figure(plot_width=plot_width_pix,
               plot_height=plot_height_pix,
               title=plot_title,
               tools=plot_tools,
               tooltips=plot_tooltips)

    p.scatter(plot_x, plot_y,
              source=df,
              fill_alpha=0.6,
              line_alpha=0,
              size=12,
              legend_group=plot_category,
              fill_color=cmap)

    p.xaxis.axis_label = plot_x
    p.yaxis.axis_label = plot_y
    p.legend.location = legend_location
    # p.legend.click_policy="mute"

    show(p)