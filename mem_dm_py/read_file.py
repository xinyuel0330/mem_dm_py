import pandas as pd


def import_data(path):
    """
    Loads csv data fire with path and file name
    :param path: path and file name in string
    :return: dataframe
    """
    df = pd.read_csv(path, sep=',', engine='python')
    return df

def save_data(df, name):
    """
    Saves dataframe into csv file
    :param df: dataframe
    :param name: file name
    :return: csv file
    """
    df.to_csv(name)
    print(f'Saved to {name}')
