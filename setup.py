import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mem_dm_py",
    version="0.0.3",
    author="Xinyue Li",
    author_email="xinyuel@uchicago.edu",
    description="A demo package for PSYC46050 final assignment",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_repquires=[
        'pandas',
        'matplotlib',
        'numpy',
        'scipy',
        'scikit-learn',
        'seaborn'
    ],
)